# Integrate Team Refinement Bot

This bot helps the Integrate team with its [backlog refinement](https://about.gitlab.com/handbook/engineering/development/dev/manage/import/#refinement).

## Set up

Clone this repository locally and install the gems:

```bash
bundle
```

## Running

Identify issues for refinement by applying the `~"ready for next refinement"` label to them, then run the following command to have the bot create a refinement issue.

### Dry run test

You can test what will happen with the `--dry-run` option:

```bash
bundle exec gitlab-triage --dry-run --token <api-token> --source-id gitlab-org/gitlab
```

### Generate the issue

To create the issue, run the same command as above without `--dry-run`.

### API token

The GitLab Bot API token is in the 1Password Engineering vault as a
Secure Note titled _Integrations Backlog Refinement GitLab Bot API Token_.

## Anyone can contribute!

See the `.triage-policies.yml` file and [`gitlab-triage` docs](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage).
